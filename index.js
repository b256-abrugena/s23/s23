// console.log("Testing");

let pokemonTrainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Balbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"],
	},
	talk: function(){
		console.log(this.pokemon[0] + "! I choose you!");
	}
};

console.log(pokemonTrainer);

let trainerName = pokemonTrainer.name;
console.log("Result of dot notation:");
console.log(trainerName);

let trainerPokemon = pokemonTrainer['pokemon'];
console.log("Result of square bracket notation:");
console.log(trainerPokemon);

console.log("Result of talk method");
pokemonTrainer.talk();


//Constructor

function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = this.level * 2;
	this.attack = this.level;
}

let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("Mewtwo", 100);

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);


Pokemon.tackle = function(usedPokemon, targetPokemon) {
	console.log(usedPokemon.name + " tackled " + targetPokemon.name);
	let hpRemain = targetPokemon.health - usedPokemon.attack;

	if(hpRemain <= 0){
		console.log(targetPokemon.name + "'s health is now reduced to " + hpRemain);
		console.log(targetPokemon.name + " fainted");
	}
	else{
		console.log(targetPokemon.name + "'s health is now reduced to " + hpRemain);
	}
}

Pokemon.tackle(geodude, pikachu);
Pokemon.tackle(mewtwo, geodude);
Pokemon.tackle(mewtwo, pikachu);









